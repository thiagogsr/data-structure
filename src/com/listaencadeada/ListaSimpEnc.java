package com.listaencadeada;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

public class ListaSimpEnc<T> implements IListaSimpEnc<T> {
	
	private NoSimpEnc<T> inicio;
	private NoSimpEnc<T> fim;
	private int count;
	
	public ListaSimpEnc() {
		this.inicio = null;
		this.fim = null;
		this.count = 0;
	}

	@Override
	public NoSimpEnc<T> getInicio() {
		return this.inicio;
	}

	@Override
	public NoSimpEnc<T> getFim() {
		return this.fim;
	}

	@Override
	public void clear() {
		this.inicio = null;
		this.fim = null;
		this.count = 0;
	}

	@Override
	public int size() {
		return this.count;
	}

	@Override
	public boolean estaVazia() {
		return this.inicio == null;
	}

	@Override
	public void InserirInicio(T elemento) {
		NoSimpEnc<T> novo = new NoSimpEnc<T>(elemento);
		if (estaVazia()) {
			inicio = fim = novo;
		} else {
			novo.setProximo(inicio);
			inicio = novo;
		}
		count++;
	}

	@Override
	public void InserirFim(T elemento) {
		NoSimpEnc<T> novo = new NoSimpEnc<T>(elemento);
		if (estaVazia()) {
			inicio = fim = novo;
		} else {
			fim.setProximo(novo);
			fim = novo;
		}
		count++;
	}

	@Override
	public void Inserir(int pos, T elemento) {
		NoSimpEnc<T> novo = new NoSimpEnc<T>(elemento);
		if (estaVazia()) {
			inicio = fim = novo;
		} else {
			if (pos == 0) {
				novo.setProximo(inicio);
				inicio = novo;
			} else if (pos == this.size()) {
				fim.setProximo(novo);
				fim = novo;
			} else {
				NoSimpEnc<T> current = inicio;
				for (int i = 0; i < pos - 1; i++) {
					current = current.getProximo();
				}

				novo.setProximo(current.getProximo());
				current.setProximo(novo);
			}
		}
		count++;
	}

	@Override
	public boolean contem(T elemento) {
		NoSimpEnc<T> current = this.inicio;
		boolean encontrou = false;
		for (int i = 0; i < count; i++) {
			if (current.getElemento().equals(elemento)) {
				encontrou = true;
				break;
			}
			current = current.getProximo();
		}
		return encontrou;
	}

	@Override
	public void RemoverInicio() {
		if (!estaVazia()) {
			inicio = inicio.getProximo();
			count--;
		}
	}

	@Override
	public void RemoverFim() {
		if (!estaVazia()) {
			NoSimpEnc<T> no = inicio;
			for (int i = 0; i < count - 2; i++) {
				no = no.getProximo();
			}
			no.setProximo(null);
			fim = no;
			count--;
		}
	}

	@Override
	public void Inserir(T objeto, T elemento) {
		if (!estaVazia()) {
			NoSimpEnc<T> no = inicio;
			NoSimpEnc<T> novo = new NoSimpEnc<T>(elemento);
			for (int i = 0; i < count; i++) {
				NoSimpEnc<T> proximo = i == 0 ? no : no.getProximo();
				if (proximo.getElemento().equals(objeto)) {
					novo.setProximo(proximo);
					if (i == 0) {
						inicio = novo;
					} else {
						no.setProximo(novo);
					}
					count++;
					break;
				}
				no = no.getProximo();
			}
		}
	}

	@Override
	public void Inserir(int pos, ListaSimpEnc<T> lista) {
		if (estaVazia()) {
			inicio = lista.inicio;
			fim = lista.fim;
			count = lista.size();
		} else {
			NoSimpEnc<T> current = inicio;
			for (int i = 0; i < pos - 1; i++) {
				current = current.getProximo();
			}
			lista.fim.setProximo(current.getProximo());
			current.setProximo(lista.inicio);
			count += lista.size();
		}
	}
	
	@Override
	public String toString() {
		String result = "[";
		NoSimpEnc<T> current = inicio;
		for (int i = 0; i < this.count; i++) {
			result += current;
			if (i < this.count - 1) {
				result += ", "; 
			}
			current = current.getProximo();
		}
		result += "]";
		return result;
	}

	@Override
	public void inverterLista() {
		if (!estaVazia()) {
			NoSimpEnc<T> current = this.inicio;
			NoSimpEnc<T> bkp = current;
			for (int i = 0; i < this.size(); i++) {
				NoSimpEnc<T> proximo = current.getProximo();
				current.setProximo(bkp);
				bkp = current;
				current = proximo;
			}
			NoSimpEnc<T> bkpInicio = this.inicio;
			this.inicio = this.fim;
			this.fim = bkpInicio;
		}
	}

	@Override
	public boolean comparaLista(ListaSimpEnc<T> lista) {
		boolean result = true;
		if (this.size() == lista.size()) {
			NoSimpEnc<T> current = this.inicio;
			NoSimpEnc<T> currentLista = lista.inicio;
			for (int i = 0; i < this.size(); i++) {
				if (!current.getElemento().equals(currentLista.getElemento())) {
					result = false;
					break;
				}
				currentLista = currentLista.getProximo();
				current = current.getProximo();
			}
		} else {
			result = false;
		}
		return result;
	}

}