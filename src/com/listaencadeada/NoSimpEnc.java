package com.listaencadeada;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

public class NoSimpEnc<T> {

	private T elemento;
	private NoSimpEnc<T> proximo;

	public NoSimpEnc(T objeto) {
		this.elemento = objeto;
		this.proximo = null;
	}

	public NoSimpEnc(T objeto, NoSimpEnc<T> proximo) {
		this.elemento = objeto;
		this.proximo = proximo;
	}

	public T getElemento() {
		return elemento;
	}

	public NoSimpEnc<T> getProximo() {
		return proximo;
	}
	
	public void setProximo(NoSimpEnc<T> objeto) {
		this.proximo = objeto;
	}
	
	@Override
	public String toString() {
		return elemento.toString();
	}

}
