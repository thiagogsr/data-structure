package com.hash;

public class Hash<T> {

	private int key;
	private T element;
	private boolean deleted;
	
	public Hash(int key, T element){
		this.key = key;
		this.element = element;
		this.deleted = false;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public int getKey() {
		return key;
	}

	public T getElement() {
		return element;
	}

}
