package com.hash;

public class HashDuplo<T> implements IHashDuplo<T>{

	private Hash[] hash;
	private int tamanho;
	
	public HashDuplo(int init){
		this.hash = new Hash[init];
		this.tamanho = 0;
	}

	@Override
	public void clear() {
		this.tamanho = 0;
		for (int i = 0; i < hash.length; i++) {
			hash[i] = null;
		}
	}
	
	public boolean estavazio(){
		return this.tamanho == 0;
	}

	@Override
	public boolean add(T obj) {
		// Alteracao para questao dois da prova de ED1 da segunda unidade
		if (this.contains(obj)) {
			return false;
		}
		
		this.escalar();

		int key = obj.hashCode();
		Hash objHash = new Hash(key, obj);
		int idx = funcHash(key);
		if (hash[idx] == null || hash[idx].isDeleted()) {
			hash[idx] = objHash;
			this.tamanho++;
			return true;
		} 
		int i = 1;
		idx = (i * funcHash2(key)) ;
		idx = idx % hash.length;
		while (hash[idx] != null) {
			if (hash[idx].isDeleted()){
				break;
			}
			i++;
			idx += (i * funcHash2(key));
			idx = idx % hash.length;
		}
		hash[idx] = objHash;
		this.tamanho++;
		return true;
	}
	
	public int idxElement(T elemento){
		int key = elemento.hashCode();
		int idx = funcHash(key);
		if (hash[idx] == null || this.tamanho == 0) {
			return -1;
		}
		int i = 1;
		while (hash[idx] != null) {
			if (hash[idx].getElement().equals(elemento) && !hash[idx].isDeleted()) {
				return idx;
			}
			idx += (i * funcHash2(key));
			idx = idx % hash.length;
			i++;
		}
		return -1;
	}

	@Override
	public boolean remove(T obj) {
		int idx = idxElement(obj);
		if (idx == -1) {
			return false;
		} 
		hash[idx].setDeleted(true);
		this.tamanho--;
		return true;
	}

	@Override
	public boolean contains(T obj) {
		int key = obj.hashCode();
		int idx = funcHash(key);
		if (hash[idx] == null) {
			return false;
		}
		int i = 1;
		while (hash[idx] != null) {
			if (hash[idx].getElement().equals(obj) && !hash[idx].isDeleted()) {
				return true;
			}
			i++;
			idx += (i * funcHash2(key));
			idx = idx % hash.length;
		}
		return false;
	}

	@Override
	public int funcHash(int key) {
		return (key % hash.length);
	}
	
	public int funcHash2(int key) {
		return (1 + (key % (hash.length - 1)));
	}
	
	private void escalar() {
		int capacidade = hash.length;
		double carga = (double) (this.tamanho / capacidade);
		if (carga > 0.75) {
			resize(capacidade * 2);
		}
	}
	
	public int size() {
		return this.tamanho;
	}
	
	private void resize(int length) {
		Hash[] aux =  new Hash[hash.length];
		for (int i = 0; i < hash.length; i++) {
			aux[i] = hash[i];
		}
		hash = new Hash[length];
		this.tamanho = 0;
		for (int i = 0; i < aux.length; i++) {
			if (aux[i] != null && !aux[i].isDeleted()){
				this.add((T) aux[i].getElement());
			}
		}
	}
	
	@Override
	public String toString() {
		if (estavazio()) {
			return "[]";
		}
		String result = "[";
		T elemento;
		for (int i = 0; i < hash.length; i++) {
			if (hash[i] == null) {
				elemento = (T) "-";
			} else if (hash[i].isDeleted()) {
				elemento = (T) "D";
			} else {
				elemento = (T) hash[i].getElement();
			}
			
			if (i == 0) {
				result += " " + elemento;
			} else {
				result += ", " + elemento;
			}
			
		}
		result += "]";
		return result;
	}
}
