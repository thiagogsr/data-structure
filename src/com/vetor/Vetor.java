package com.vetor;

import java.lang.reflect.Array;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

public class Vetor<T extends Comparable<T>> implements IVetor<T> {
	
	private int size;
	private T[] vetor;
	
	public Vetor(int init) {
		this.size = 0;
		this.vetor = (T[]) new Object[init];
	}

	public Vetor(Class<T> c, int init) {
		this.size = 0;
		this.vetor = (T[]) Array.newInstance(c, init);
	}

	private int searchelemento(T elemento) {
		int result = -1;
		for (int i = 0; i < this.vetor.length; i++) {
			if (this.vetor[i] != null && this.vetor[i].equals(elemento)) {
				result = i;
				break;
			}
		}
		return result;
	}
	
	@Override
	public void Adicionar(T elemento) {
		if (elemento != null) {
			this.escalarVetor();
			this.vetor[this.size] = elemento;
			this.size++;
		}
	}
	
	@Override
	public void Adicionar(int position, T elemento) throws ArrayIndexOutOfBoundsException {
		try {
			if (elemento != null) {
				this.size++;
				T bkpelemento = this.vetor[position];
				if (bkpelemento != null) {
					for (int i = position; i < (this.vetor.length-1); i++) {
						T elTemp = bkpelemento;
						bkpelemento = this.vetor[i+1];
						this.vetor[i+1] = elTemp;
					}
				}
				System.out.println(toString());
				this.vetor[position] = elemento;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(e.getMessage());
		}
	}
	
	@Override
	public void AdicionarInicio(T elemento) {
		if (elemento != null) {
			this.escalarVetor();

			T bkpelemento = this.vetor[0];
			if (bkpelemento != null) {
				for (int i = 0; i < (this.vetor.length-1); i++) {
					T elTemp = bkpelemento;
					bkpelemento = this.vetor[i+1];
					this.vetor[i+1] = elTemp;
				}
				this.size++;
			}
			
			this.vetor[0] = elemento;
		}
	}

	@Override
	public void Inserir(int pos, T[] lista) {
		for (int i = 0; i < lista.length; i++) {
			this.escalarVetor();
			int position = pos+i;
			T bkpelemento = this.vetor[position];
			for (int c = position; c < this.Tamanho(); c++) {
				T elTemp = bkpelemento;
				bkpelemento = this.vetor[c+1];
				this.vetor[c+1] = elTemp;
			}
			this.vetor[position] = lista[i];
			this.size++;
		}
	}
	
	@Override
	public void Remover(T elemento) {
		if (elemento != null) {
			int elPosition = this.searchelemento(elemento);
			if (elPosition != -1) {
				this.vetor[elPosition] = null;
			}
			
			this.reorganizarVetor();
			
			this.size--;
		}
	}
	
	@Override
	public void Remover(int position) throws ArrayIndexOutOfBoundsException {
		try {
			this.vetor[position] = null;
			this.reorganizarVetor();
			this.size--;
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(e.getMessage());
		}
	}
	
	@Override
	public void RemoverInicio() {
		if (this.size > 0 && this.vetor[0] != null) {
			this.vetor[0] = null;
			this.size--;
			this.reorganizarVetor();
		}
	}
	
	@Override
	public void RemoverFim() {
		if (this.size > 0 && this.vetor[(this.size-1)] != null) {
			this.vetor[(this.size-1)] = null;
			this.size--;
			this.reorganizarVetor();
		}
	}

	@Override
	public int Tamanho() {
		return this.size;
	}
	
	@Override
	public void Limpar() {
		for (int i = 0; i < this.vetor.length; i++) {
			this.vetor[i] = null;
		}
		this.size = 0;
	}

	@Override
	public boolean contem(T elemento) {
		return this.searchelemento(elemento) != -1;
	}

	private void escalarVetor() {
		if (this.size == this.vetor.length) {
			T newVetor[] = (T[]) new Object[this.size*2];
			for (int i = 0; i < this.size; i++) {
				newVetor[i] = this.vetor[i];
			}
			this.vetor = newVetor;
		}
	}

	private void reorganizarVetor() {
		for (int i = 0; i < (this.vetor.length-1); i++) {
			if (this.vetor[i] == null && this.vetor[i+1] != null) {
				this.vetor[i] = this.vetor[i+1];
				this.vetor[i+1] = null;
			}
		}
	}

	public void bubbleSort() {
		int t = this.Tamanho();
		for (int i = 0; i < t-1; i++) {
			for (int j = 1; j < t-i; j++) {
				if (this.vetor[j].compareTo((T) this.vetor[j-1]) < 0) {
					Integer bkp = (Integer) this.vetor[j];
					this.vetor[j] = this.vetor[j-1];
					this.vetor[j-1] = (T) bkp;
				}
			}
		}
	}

	public void selectionSort() {
		int t = this.Tamanho();
		for (int i = 0; i < t-1; i++) {
			int idx = i;
			for (int j = i+1; j < t; j++) {
				if (this.vetor[j].compareTo((T) this.vetor[idx]) < 0) {
					idx = j;
				}
			}
			Integer bkp = (Integer) this.vetor[i];
			this.vetor[i] = this.vetor[idx];
			this.vetor[idx] = (T) bkp;
		}
	}
	
	public void insertionSort() {
		T aux;
		int t = this.Tamanho();
		for (int i = 1; i < t; i++){
			aux = this.vetor[i];
			for (int j = i; j > 0; j--){
				if (this.vetor[j].compareTo((T) this.vetor[j-1]) < 0) {
					vetor[j] = vetor[j-1];
					vetor[j-1] = aux;
				}
			}
		}
	}
	
	public String print(T[] vetor2) {
		String result = "[";
		if (vetor2.length > 0) {
			for (int i = 0; i < vetor2.length; i++) {
				result += vetor2[i];
				if ((i+1) < vetor2.length) {
					result += ", ";
				}
			}
		}
		result += "]";
		return result;
	}

	public String toString() {
		return print(this.vetor);
	}

	public T get(int i) {
		return this.vetor[i];
	}

}
