package com.vetor;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

public interface IIterator<T> {
	
	public boolean hasNext();
	public T next();

}
