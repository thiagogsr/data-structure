package com.vetor;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

public interface IVetor<T> {
	
	public void Adicionar(T elemento);
	public void Adicionar(int posicao, T elemento) throws ArrayIndexOutOfBoundsException;
	public void AdicionarInicio(T elemento);
	public void Inserir(int pos, T[] lista);
	public void Remover(int posicao) throws ArrayIndexOutOfBoundsException;
	public void Remover(T elemento);
	public void RemoverInicio();
	public void RemoverFim();
	public int Tamanho();
	public void Limpar();
	public boolean contem(T elemento);
	public String toString();
	
}