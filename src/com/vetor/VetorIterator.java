package com.vetor;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

public class VetorIterator<T extends Comparable<T>> implements IIterator<T> {

	private Vetor<T> vetor;
	private int currentPosition;
	
	public VetorIterator(Vetor<T> vetor) {
		this.vetor = vetor;
		this.currentPosition = 0;
	}

	@Override
	public boolean hasNext() {
		return this.vetor.get(currentPosition) != null;
	}

	@Override
	public T next() {
		return this.vetor.get(currentPosition++);
	}

}
