package com.exceptions;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

public class PilhaCheiaException extends Exception {
	
	public PilhaCheiaException() {
		super("A pilha está cheia. Não é possível inserir mais elementos.");
	}

}
