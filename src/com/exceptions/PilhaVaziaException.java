package com.exceptions;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

public class PilhaVaziaException extends Exception {
	
	public PilhaVaziaException() {
		super("A pilha está vazia.");
	}

}
