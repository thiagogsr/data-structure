package com.exceptions;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

public class FilaVaziaException extends Exception {
	
	public FilaVaziaException() {
		super("A fila está vazia.");
	}

}
