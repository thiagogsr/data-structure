package com.exceptions;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

public class FilaCheiaException extends Exception {
	
	public FilaCheiaException() {
		super("A fila está cheia. Não é possível inserir mais elementos.");
	}

}
