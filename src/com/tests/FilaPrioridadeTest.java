package com.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.exceptions.FilaCheiaException;
import com.exceptions.FilaVaziaException;
import com.fila.FilaVetorPrioridade;

public class FilaPrioridadeTest {

	private FilaVetorPrioridade<Integer> fila;
	
	@Before
	public void Iniciar() {
		fila = new FilaVetorPrioridade<Integer>(Integer.class, 5);
	} 
	
	@Test
	public void TestaTamanho() throws FilaCheiaException, FilaVaziaException {
		fila.Enfileirar(10);
		fila.Enfileirar(30);
		fila.Enfileirar(20);
		
		assertEquals(3, fila.size());
	}
	
	@Test
	public void TestaEnfileirar() throws FilaCheiaException {
		fila.Enfileirar(10);
		fila.Enfileirar(30);
		fila.Enfileirar(20);
		fila.Enfileirar(40);
		
		assertEquals("[40,30,20,10]", fila.toString());
	}
	
	
	@Test
	public void TestaRemover() throws FilaVaziaException, FilaCheiaException {
		fila.Enfileirar(10);
		fila.Enfileirar(30);
		fila.Enfileirar(20);
		fila.Enfileirar(40);
		fila.Remover();
		
		assertEquals("[30,20,10]",fila.toString());
	}
	
	@Test
	public void TestaRemover2() throws FilaVaziaException, FilaCheiaException {
		fila.Enfileirar(10);
		fila.Enfileirar(30);
		fila.Enfileirar(20);
		fila.Enfileirar(40);
		
		fila.Remover();
		fila.Remover();
		fila.Remover();
		fila.Remover();
		
		assertEquals("[]",fila.toString());
	}
	
	@Test
	public void TestaVetorCircular() throws FilaVaziaException, FilaCheiaException {
		fila.Enfileirar(10);
		fila.Enfileirar(30);
		fila.Enfileirar(20);
		fila.Enfileirar(40);
		
		fila.Remover();
		fila.Enfileirar(50);
		fila.Enfileirar(60);
			
		assertEquals("[60,50,30,20,10]",fila.toString());
	}
	
	@Test
	public void TestaVetorCircular2() throws FilaVaziaException, FilaCheiaException {
		fila.Enfileirar(10);
		fila.Enfileirar(30);
		fila.Enfileirar(20);
		fila.Enfileirar(40);
		fila.Remover();
		fila.Remover();
		fila.Enfileirar(50);
		fila.Enfileirar(60);
		fila.Enfileirar(70);
		
		assertEquals("[70,60,50,20,10]", fila.toString());
	}
	
	
	@Test(expected = FilaVaziaException.class)
	public void TestaFilaVazia() throws FilaVaziaException {
		fila.Remover();
	}

	@Test(expected = FilaCheiaException.class)
	public void TestaFilaCheia() throws FilaCheiaException, FilaVaziaException {
		fila.Enfileirar(10);
		fila.Enfileirar(30);
		fila.Enfileirar(20);
		fila.Enfileirar(40);
		fila.Remover();
		fila.Remover();
		fila.Enfileirar(50);
		fila.Enfileirar(60);
		fila.Enfileirar(70);
		fila.Enfileirar(90);
	}

	@Test
	public void TestaContem() throws FilaCheiaException {
		fila.Enfileirar(10);
		fila.Enfileirar(30);
		fila.Enfileirar(40);
		assertEquals(true, fila.contem(30));
	}

	@Test
	public void TestaContem2() throws FilaCheiaException {
		fila.Enfileirar(10);
		fila.Enfileirar(30);
		fila.Enfileirar(40);
		assertEquals(true, fila.contem(10));
	}

	@Test
	public void TestaContem3() throws FilaCheiaException {
		fila.Enfileirar(10);
		assertEquals(true, fila.contem(10));
	}

	@Test
	public void TestaNaoContem() throws FilaVaziaException, FilaCheiaException {
		fila.Enfileirar(10);
		fila.Enfileirar(30);
		fila.Enfileirar(40);
		fila.Remover();
		assertEquals(false, fila.contem(40));
	}

	@Test
	public void TestaNaoContem2() throws FilaVaziaException, FilaCheiaException {
		fila.Enfileirar(10);
		fila.Remover();
		assertEquals(false, fila.contem(10));
	}

}
