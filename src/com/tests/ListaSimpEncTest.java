package com.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.listaencadeada.*;

public class ListaSimpEncTest {

	ListaSimpEnc<Integer> lista;
	
	@Before
	public void Iniciar() {
		lista = new ListaSimpEnc<Integer>();
	}

	@Test
	public void InserirNoInicioTest() {
		lista.InserirInicio(9);
		assertEquals(1, lista.size());
	}

	@Test
	public void ContemTest() {
		lista.InserirInicio(9);
		lista.InserirInicio(6);
		lista.InserirInicio(2);
		lista.InserirInicio(1);

		assertEquals(true, lista.contem(2));
	}

	@Test
	public void NaoContemTest() {
		lista.InserirInicio(9);
		lista.InserirInicio(6);
		lista.InserirInicio(2);
		lista.InserirInicio(1);

		assertEquals(false, lista.contem(10));
	}

	@Test
	public void InserirVariosInicioTest() {
		lista.InserirInicio(9);
		lista.InserirInicio(6);
		lista.InserirInicio(2);
		lista.InserirInicio(1);
		assertEquals("[1, 2, 6, 9]", lista.toString());
	}

	@Test
	public void InserirVariosFimTest() {
		lista.InserirInicio(6);
		lista.InserirInicio(2);
		lista.InserirInicio(1);
		lista.InserirFim(9);
		assertEquals("[1, 2, 6, 9]", lista.toString());
	}

	@Test
	public void InserirDoisFimTest() {
		lista.InserirFim(9);
		lista.InserirFim(2);
		assertEquals("[9, 2]", lista.toString());
	}

	@Test
	public void InserirTresFimTest() {
		lista.InserirFim(9);
		lista.InserirFim(2);
		lista.InserirFim(1);
		assertEquals("[9, 2, 1]", lista.toString());
	}

	@Test
	public void InserirPosTest() {
		lista.InserirFim(9);
		lista.InserirFim(2);
		lista.InserirFim(1);
		lista.Inserir(1, (Integer) 6);
		assertEquals("[9, 6, 2, 1]", lista.toString());
	}

	@Test
	public void InserirPosFimTest() {
		lista.InserirFim(9);
		lista.InserirFim(2);
		lista.InserirFim(1);
		lista.Inserir(3, (Integer) 6);
		assertEquals("[9, 2, 1, 6]", lista.toString());
	}

	@Test
	public void RemoverInicioTest() {
		lista.InserirFim(9);
		lista.InserirFim(2);
		lista.InserirFim(1);
		lista.RemoverInicio();
		assertEquals("[2, 1]", lista.toString());
	}

	@Test
	public void RemoverUnicoTest() {
		lista.InserirFim(9);
		lista.RemoverInicio();
		assertEquals("[]", lista.toString());
	}

	@Test
	public void RemoverTresTest() {
		lista.InserirFim(2);
		lista.InserirFim(1);
		lista.InserirInicio(9);
		lista.RemoverInicio();
		assertEquals("[2, 1]", lista.toString());
	}

	@Test
	public void RemoverFimTest() {
		lista.InserirFim(9);
		lista.InserirFim(2);
		lista.InserirFim(1);
		lista.RemoverFim();
		assertEquals("[9, 2]", lista.toString());
	}

	@Test
	public void RemoverFimUnicoTest() {
		lista.InserirFim(9);
		lista.RemoverFim();
		assertEquals("[]", lista.toString());
	}

	@Test
	public void InserirElementoTest() {
		lista.InserirFim(9);
		lista.Inserir(0, (Integer) 3);
		assertEquals("[3, 9]", lista.toString());
	}
	
	@Test
	public void InserirElementoFimTest() {
		lista.InserirFim(9);
		lista.InserirFim(3);
		lista.InserirFim(5);
		lista.Inserir(2, (Integer) 6);
		assertEquals("[9, 3, 6, 5]", lista.toString());
	}

}
