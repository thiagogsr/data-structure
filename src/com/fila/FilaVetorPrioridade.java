package com.fila;

import java.lang.reflect.Array;

import com.exceptions.FilaCheiaException;
import com.exceptions.FilaVaziaException;

public class FilaVetorPrioridade<T extends Comparable<T>> implements IFilaPrioridade<T>{

	private T[] fila;
	private int tamanho;
	private int inicio;
	private int fim;
	
	public FilaVetorPrioridade(Class<T> c, int init){
		this.fila = (T[]) Array.newInstance(c, init);
		this.limpar();
	}
	
	@Override
	public void Enfileirar(T objeto) throws FilaCheiaException{
		if ((this.fim + 1) == this.inicio) {
			throw new FilaCheiaException();
		}

		if (this.estaVazia()) {
			this.fila[0] = objeto;
		} else if ((fim + 1) % fila.length == 0 && this.inicio != 0) {
			this.fim = 0;
			fila[0] = objeto;
			this.ordenar();
		} else {
			this.fim++;
			fila[fim] = objeto;
			this.ordenar();
		}
		this.tamanho++;
	}
	
	@Override
	public T Remover() throws FilaVaziaException {
		if (estaVazia()){
			throw new FilaVaziaException();
		}
		T elemento = fila[inicio];
		fila[inicio] = null;
		if ((this.inicio + 1) < fila.length) {
			this.inicio++;
		} else {
			this.inicio = 0;
		}
		this.tamanho--;
		return elemento;
	}
	
	public void ordenar() {
		T auxiliar;
		int indiceElemento = fim;
		int indiceAnterior;
		if (indiceElemento == 0) {
			indiceAnterior = fila.length - 1;
		} else {
			indiceAnterior = fim - 1;
		}
		for (int i = 0; i <= this.tamanho; i++) {
			if (fila[indiceElemento].compareTo(fila[indiceAnterior]) == - 1) {
				auxiliar = fila[indiceAnterior];
				fila[indiceAnterior] = fila[indiceElemento];
				fila[indiceElemento] = auxiliar;
			}
			indiceAnterior = indiceElemento;
			if (indiceElemento == 0) {
				indiceElemento = fila.length - 1;
			} else {
				indiceElemento--;
			}
		}
	}

	@Override
	public T getInicio() {
		return fila[inicio];
	}

	@Override
	public T getFim() {
		return fila[fim];
	}

	@Override
	public void limpar() {
		this.inicio = 0;
		this.fim = 0;
		this.tamanho = 0;
	}
	
	public int size() {
		return this.tamanho;
	}

	@Override
	public boolean contem(T objeto) {
		int indice = inicio;
		for (int i = 0; i < this.size(); i++) {
			if (fila[indice].equals(objeto)) {
				return true;
			}
			if ((indice + 1) == fila.length) {
				indice = 0;
			} else {
				indice++;
			}
		}
		return false;
	}
	

	@Override
	public boolean estaVazia() {
		return this.size() == 0;
	}
	
	@Override
	public String toString() {
		if (estaVazia()){
			return "[]";
		}
		String stringRetorno = "[";
		int indice = inicio;
		for (int i = 0; i < this.size(); i++) {
			if (indice == inicio) {
				stringRetorno = stringRetorno + fila[indice];
			}else{
				stringRetorno = stringRetorno + "," + fila[indice];
			}
			if ((indice + 1) == fila.length) {
				indice = 0;
			} else {
				indice++;
			}
		}
		stringRetorno = stringRetorno + "]";
		return stringRetorno;
	}

}
