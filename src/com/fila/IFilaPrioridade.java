package com.fila;

import com.exceptions.FilaCheiaException;
import com.exceptions.FilaVaziaException;

public interface IFilaPrioridade<T> {

	public void Enfileirar(T objeto) throws FilaCheiaException;
	public T Remover() throws FilaVaziaException;
	public T getInicio();
	public T getFim();
	public String toString();
	public void limpar();
	public boolean contem(T objeto);
	public boolean estaVazia();

}
