package com.fila;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

import java.lang.reflect.Array;

import com.exceptions.FilaCheiaException;
import com.exceptions.FilaVaziaException;

public class FilaVetor<T> implements IFila<T> {
	
	private T vetor[];
	private Integer inicio;
	private Integer fim;
	private Integer tamanho;
	
	public FilaVetor(Class<T> c, int init) {
		this.vetor = (T[]) Array.newInstance(c, init);
		this.inicio = 0;
		this.fim = 0;
		this.tamanho = 0;
	}

	@Override
	public void Enfileirar(T objeto) throws FilaCheiaException {
		if (this.fim + 1  == inicio) {
			throw new FilaCheiaException();
		}

		if (this.estaVazia()) {
			this.vetor[0] = objeto;
		} else if ((this.fim + 1) % this.vetor.length == 0 && this.inicio != 0) {
			this.fim = 0;
			this.vetor[0] = objeto;
		} else {
			this.fim++;
			this.vetor[fim] = objeto;
		}
		this.tamanho++;
	}

	@Override
	public T Remover() throws FilaVaziaException {
		if (this.estaVazia()) {
			throw new FilaVaziaException();
		}
		
		T elemento = this.vetor[inicio];
		this.vetor[inicio] = null;
		if ((this.inicio + 1) < this.vetor.length) {
			this.inicio++;
		} else {
			this.inicio = 0;
		}
		this.tamanho--;
		return elemento;
	}

	@Override
	public T getInicio() {
		return this.vetor[this.inicio];
	}

	@Override
	public T getFim() {
		return this.vetor[this.fim];
	}

	@Override
	public void limpar() {
		this.inicio = 0;
		this.fim = 0;
		for (int i = 0; i < this.tamanho; i++) {
			this.vetor[i] = null;
		}
		this.tamanho = 0;
	}

	@Override
	public boolean contem(T objeto) {
		int indice = this.inicio;
		for (int i = 0; i < this.tamanho; i++) {
			if (this.vetor[indice].equals(objeto)){
				return true;
			}
			if ((indice + 1) == this.vetor.length) {
				indice = 0;
			} else {
				indice++;
			}
		}
		return false;
	}
	
	public boolean estaVazia() {
		return this.size() == 0;
	}
	
	public int size() {
		return this.tamanho;
	}
	
	@Override
	public String toString() {
		String result = "[";
		if (!this.estaVazia()) {
			int idx = inicio;
			for (int i = 0; i < this.tamanho; i++) {
				result += this.vetor[idx];
				if (idx + 1 == this.vetor.length) {
					idx = 0;
				} else {
					idx++;
				}
				if ((i+1) < this.tamanho) {
					result += ",";
				}
			}
		}
		result += "]";
		return result;
	}
	
	@Override
	public String toString(boolean styled) {
		String result = "";
		if (!estaVazia()) {
			result += "-------------- ";
			for (int i = (this.inicio + 1); i < this.fim; i++) {
				result += "----- ";
			}
			result += "-----------\n";
			for (int i = this.inicio; i < this.size(); i++) {
				result += "| " + this.vetor[i];
				if (i == this.inicio) {
					result += " (início)";
				}
				if (i == this.fim) {
					result += " (fim)";
				}
				result += " | ";
			}
			result += "\n-------------- ";
			for (int i = (this.inicio + 1); i < this.fim; i++) {
				result += "----- ";
			}
			result += "-----------\n";
		}
		result += "";
		return result;
	}

}