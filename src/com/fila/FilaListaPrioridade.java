package com.fila;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

import com.exceptions.FilaVaziaException;
import com.listaencadeada.NoSimpEnc;

public class FilaListaPrioridade<T extends Comparable<T>> implements IFila<T> {
		
	private NoSimpEnc<T> fim;
	private int size;
	
	public FilaListaPrioridade() {
		this.size = 0;
	}

	@Override
	public void Enfileirar(T objeto) {
		NoSimpEnc<T> no = new NoSimpEnc<T>(objeto);
		if (this.estaVazia()) {
			this.fim = no;
			this.fim.setProximo(no);
		} else {
			if (no.getElemento().compareTo((T) this.getFim()) < 0) {
				no.setProximo(this.fim.getProximo());
				this.fim.setProximo(no);
				this.fim = no;
			} else {
				if (no.getElemento().compareTo((T) this.getInicio()) > 0) {
					no.setProximo(this.fim.getProximo());
					this.fim.setProximo(no);
				} else {
					NoSimpEnc<T> prevNo = this.noPosition(no);
					no.setProximo(prevNo.getProximo());
					prevNo.setProximo(no);
				}
			}
		}
		this.size++;
	}

	@Override
	public T Remover() throws FilaVaziaException {
		if (this.estaVazia()) {
			throw new FilaVaziaException();
		}
		
		T result = this.getInicio();
		if (this.size == 1) {
			this.limpar();
		} else {
			this.fim.setProximo(this.fim.getProximo().getProximo());
			this.size--;
		}
		return result;
	}

	@Override
	public T getInicio() {
		return this.fim.getProximo().getElemento();
	}

	@Override
	public T getFim() {
		return this.fim.getElemento();
	}

	@Override
	public void limpar() {
		this.fim = null;
		this.size = 0;
	}
	
	public boolean estaVazia() {
		return this.size == 0;
	}
	
	public NoSimpEnc<T> noPosition(NoSimpEnc<T> no) {
		NoSimpEnc<T> current = this.fim.getProximo(); // inicio
		NoSimpEnc<T> aux = current;
		for (int i = 0; i < this.size; i++) {
			if (no.getElemento().compareTo((T) current.getElemento()) > 0) {
				return aux;
			}
			aux = current;
			current = current.getProximo();
		}
		return aux;
	}

	@Override
	public boolean contem(T objeto) {
		NoSimpEnc<T> current = this.fim;
		boolean encontrou = false;
		for (int i = 0; i < this.size; i++) {
			if (current.getElemento().equals(objeto)) {
				encontrou = true;
				break;
			}
			current = current.getProximo();
		}
		return encontrou;
	}
	
	public int size() {
		return this.size;
	}
	
	@Override
	public String toString() {
		String result = "[";
		if (!estaVazia()) {
			NoSimpEnc<T> current = this.fim.getProximo();
			for (int i = 0; i < this.size; i++) {
				result += current.getElemento();
				if ((i+1) < this.size) {
					result += ", ";
				}
				current = current.getProximo();
			}
		}
		result += "]";
		return result;
	}
	
	@Override
	public String toString(boolean styled) {
		String result = "";
		if (!estaVazia()) {
			for (int i = 0; i < this.size-1; i++) {
				result += "_________";
			}
			result += "_____\n";
			NoSimpEnc<T> current = this.fim.getProximo();
			for (int i = 0; i < this.size; i++) {
				result += "| " + current.getElemento();
				if ((i+1) < this.size) {
					result += "-|--> ";
				} else {
					result += " |";
				}
				current = current.getProximo();
			}
			result += "\n";
			for (int i = 0; i < this.size-1; i++) {
				result += "---------";
			}
			result += "-----";
		}
		result += "";
		return result;
	}

}
