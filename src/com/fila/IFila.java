package com.fila;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

import com.exceptions.FilaCheiaException;
import com.exceptions.FilaVaziaException;

public interface IFila<T> {

	public void Enfileirar(T objeto) throws FilaCheiaException;
	public T Remover() throws FilaVaziaException;
	public T getInicio();
	public T getFim();
	public void limpar();
	public boolean contem(T objeto);
	public String toString();
	String toString(boolean styled);

}