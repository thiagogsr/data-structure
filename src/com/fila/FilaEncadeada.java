package com.fila;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

import com.exceptions.FilaVaziaException;
import com.listaencadeada.NoSimpEnc;

public class FilaEncadeada<T extends Comparable<T>> implements IFila<T> {
	
	private NoSimpEnc<T> fim;
	private int size;
	
	public FilaEncadeada() {
		this.size = 0;
	}

	@Override
	public void Enfileirar(T objeto) {
		NoSimpEnc<T> no = new NoSimpEnc<T>(objeto);
		if (this.estaVazia()) {
			this.fim = no;
			this.fim.setProximo(no);
		} else {
			no.setProximo(this.fim.getProximo());
			this.fim.setProximo(no);
			this.fim = no;
		}
		
		this.size++;
	}

	@Override
	public T Remover() throws FilaVaziaException {
		if (this.estaVazia()) {
			throw new FilaVaziaException();
		}
		
		T result = this.getInicio();
		if (this.size == 1) {
			this.limpar();
		} else {
			this.fim.setProximo(this.fim.getProximo().getProximo());
			this.size--;
		}
		return result;
	}

	@Override
	public T getInicio() {
		return this.fim.getProximo().getElemento();
	}

	@Override
	public T getFim() {
		return this.fim.getElemento();
	}

	@Override
	public void limpar() {
		this.fim = null;
		this.size = 0;
	}
	
	public boolean estaVazia() {
		return this.size == 0;
	}
	
	public int size() {
		return this.size;
	}

	@Override
	public boolean contem(T objeto) {
		NoSimpEnc<T> current = this.fim;
		boolean encontrou = false;
		for (int i = 0; i < this.size; i++) {
			if (current.getElemento().equals(objeto)) {
				encontrou = true;
				break;
			}
			current = current.getProximo();
		}
		return encontrou;
	}
	
	/**
	 * Metodo para primeira questao da prova da segunda unidade de ED1.
	 * Compara a lista do objeto com a lista passada como parametro e retorna a intersecao das duas.
	 * @param fila ({@link FilaEncadeada})
	 * @return {@link FilaEncadeada}
	 */
	public FilaEncadeada<T> MesmosElementos(FilaEncadeada<T> fila) {
		FilaEncadeada<T> result = new FilaEncadeada<T>();
		
		NoSimpEnc<T> current = this.fim.getProximo();
		for (int i = 0; i < this.size(); i++) {
			NoSimpEnc<T> filaCurrent = fila.fim.getProximo();
			for (int c = 0; c < fila.size(); c++) {
				if (filaCurrent.getElemento().equals(current.getElemento())) {
					result.Enfileirar(filaCurrent.getElemento());
				}
				filaCurrent = filaCurrent.getProximo();
			}
			current = current.getProximo();
		}
		return result;
	}
	
	@Override
	public String toString() {
		String result = "[";
		if (!estaVazia()) {
			NoSimpEnc<T> current = this.fim.getProximo();
			for (int i = 0; i < this.size; i++) {
				result += current.getElemento();
				if ((i+1) < this.size) {
					result += ",";
				}
				current = current.getProximo();
			}
		}
		result += "]";
		return result;
	}
	
	@Override
	public String toString(boolean styled) {
		String result = "";
		if (!estaVazia()) {
			for (int i = 0; i < this.size-1; i++) {
				result += "_________";
			}
			result += "_____\n";
			NoSimpEnc<T> current = this.fim.getProximo();
			for (int i = 0; i < this.size; i++) {
				result += "| " + current.getElemento();
				if ((i+1) < this.size) {
					result += "-|--> ";
				} else {
					result += " |";
				}
				current = current.getProximo();
			}
			result += "\n";
			for (int i = 0; i < this.size-1; i++) {
				result += "---------";
			}
			result += "-----";
		}
		result += "";
		return result;
	}

}
