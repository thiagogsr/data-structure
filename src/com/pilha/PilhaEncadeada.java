package com.pilha;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

import com.exceptions.PilhaVaziaException;
import com.listaencadeada.NoSimpEnc;

public class PilhaEncadeada<T> implements IPilha<T> {
	
	private NoSimpEnc<T> topo;
	int size;

	@Override
	public void empilhe(T objeto) {
		NoSimpEnc<T> no = new NoSimpEnc<T>(objeto);
		no.setProximo(topo);
		this.topo = no;
		this.size++;
	}

	@Override
	public T desempilhe() throws PilhaVaziaException {
		if (estaVazia()) {
			throw new PilhaVaziaException();
		}
		
		T result = this.topo.getElemento();
		this.topo = this.topo.getProximo();
		this.size--;
		return result;
	}

	@Override
	public T getTopo() throws PilhaVaziaException {
		if (estaVazia()) {
			throw new PilhaVaziaException();
		}
		
		return this.topo.getElemento();
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean estaVazia() {
		return this.size() == 0;
	}

	@Override
	public boolean contem(T objeto) {
		NoSimpEnc<T> current = this.topo;
		boolean encontrou = false;
		for (int i = 0; i < this.size; i++) {
			if (current.getElemento().equals(objeto)) {
				encontrou = true;
				break;
			}
			current = current.getProximo();
		}
		return encontrou;
	}
	
	@Override
	public String toString() {
		String result = "[";
		if (!this.estaVazia()) {
			NoSimpEnc<T> current = this.topo;
			int idx = (this.size()-1);
			for (int i = idx; i >= 0; i--) {
				result += current.getElemento();
				if ((i-1) > -1) {
					result += ", ";
				}
				current = current.getProximo();
			}
		}
		result += "]";
		
		return result;
	}
	
	@Override
	public String toString(boolean styled) {
		String result = "  _ \n_|_|_\n";
		if (!this.estaVazia()) {
			NoSimpEnc<T> current = this.topo;
			int topo = (this.size()-1);
			for (int i = topo; i >= 0; i--) {
				result += "| " + current.getElemento() + " |";
				if (i == topo) {
					result += " <-- topo";
				}
				result += "\n";
				current = current.getProximo();
			}
		}
		result += "-----\n |_| ";
		
		return result;
	}

}
