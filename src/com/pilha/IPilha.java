package com.pilha;

import com.exceptions.PilhaVaziaException;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

public interface IPilha<T> {

	public void empilhe(T objeto);
	public T desempilhe() throws PilhaVaziaException;
	public T getTopo() throws PilhaVaziaException;
	public int size();
	public boolean estaVazia();
	public boolean contem(T objeto);
	public String toString();
	String toString(boolean styled);

}