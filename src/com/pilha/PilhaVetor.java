package com.pilha;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

import com.exceptions.PilhaVaziaException;
import com.vetor.Vetor;

public class PilhaVetor<T extends Comparable<T>> implements IPilha<T> {
	
	private Vetor<T> vetor;
	private Integer topo;
	
	public PilhaVetor(int init) {
		 this.vetor = new Vetor<T>(init);
		 this.topo = -1;
	}
	
	public PilhaVetor(Class<T> c, int init) {
		 this.vetor = new Vetor<T>(c, init);
		 this.topo = -1;
	}

	@Override
	public void empilhe(T objeto) {
		this.vetor.Adicionar(objeto);
		this.topo++;
	}

	@Override
	public T desempilhe() throws PilhaVaziaException {
		if (estaVazia()) {
			throw new PilhaVaziaException();
		}
		T result = this.getTopo();
		this.vetor.RemoverFim();
		this.topo--;
		
		return result;
	}

	@Override
	public T getTopo() throws PilhaVaziaException {
		if (estaVazia()) {
			throw new PilhaVaziaException();
		}
		
		return this.vetor.get(this.topo);
	}

	@Override
	public int size() {
		return this.topo + 1;
	}

	@Override
	public boolean estaVazia() {
		return this.size() == 0;
	}

	@Override
	public boolean contem(T objeto) {
		return this.vetor.contem(objeto);
	}
	
	/**
	 * Metodo para terceira questao da prova de ED1 da segunda unidade.
	 * Recebe uma pilha e retorna uma pilha ordenada com os elementos das duas pilhas
	 * @param p {@link PilhaVetor}
	 * @return {@link PilhaVetor}
	 */
	public PilhaVetor<T> ordenaPilha(PilhaVetor<T> p, Class<T> c) {
		PilhaVetor<T> result = new PilhaVetor<T>(c, 10);

		for (int i = this.topo; i >= 0; i--) {
			result.empilhe(this.vetor.get(i));
		}

		for (int i = p.topo; i >= 0; i--) {
			result.empilhe(p.vetor.get(i));
		}

		result.vetor.insertionSort();
		result.topo = result.vetor.Tamanho() - 1;

		return result;
	}
	
	/**
	 * toString especial para a prova da segunda unidade de ED1
	 */
	@Override
	public String toString() {
		String result = "[";
		if (!this.estaVazia()) {
			for (int i = 0; i < this.size(); i++) {
				result += this.vetor.get(i);
				if ((i+1) < this.size()) {
					result += ", ";
				}
			}
		}
		result += "]";
		
		return result;
	}
	
//	@Override
//	public String toString() {
//		String result = "[";
//		if (!this.estaVazia()) {
//			for (int i = this.topo; i >= 0; i--) {
//				result += this.vetor.get(i);
//				if ((i-1) > -1) {
//					result += ", ";
//				}
//			}
//		}
//		result += "]";
//		
//		return result;
//	}
	
	@Override
	public String toString(boolean styled) {
		String result = "  _ \n_|_|_\n";
		if (!this.estaVazia()) {
			for (int i = this.topo; i >= 0; i--) {
				result += "| " + this.vetor.get(i) + " |";
				if (i == this.topo) {
					result += " <-- topo";
				}
				result += "\n";
			}
		}
		result += "-----\n |_| ";
		
		return result;
	}

}
