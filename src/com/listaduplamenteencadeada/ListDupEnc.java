package com.listaduplamenteencadeada;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

public class ListDupEnc<T> implements IListDupEnc<T> {
	
	private NoDupEnc<T> inicio;
	private NoDupEnc<T> fim;
	private int count;
	
	public ListDupEnc() {
		this.inicio = null;
		this.fim = null;
		this.count = 0;
	}

	@Override
	public NoDupEnc<T> getInicio() {
		return this.inicio;
	}

	@Override
	public NoDupEnc<T> getFim() {
		return this.fim;
	}

	@Override
	public void clear() {
		this.inicio = null;
		this.fim = null;
		this.count = 0;
	}

	@Override
	public int size() {
		return this.count;
	}

	@Override
	public boolean estaVazia() {
		return inicio == null;
	}

	@Override
	public void InserirInicio(T elemento) {
		NoDupEnc<T> no = new NoDupEnc<T>(elemento);
		this.inicio.setAnterior(no);
		no.setProximo(this.inicio);
		this.inicio = no;
		this.count++;
	}

	@Override
	public void InserirFim(T elemento) {
		NoDupEnc<T> no = new NoDupEnc<T>(elemento);
		if (estaVazia()) {
			this.inicio = this.fim = no;
		} else {
			this.fim.setProximo(no);
			no.setAnterior(this.fim);
			this.fim = no;
		}
		this.count++;
	}

	@Override
	public void Inserir(int pos, T elemento) {
		NoDupEnc<T> no = new NoDupEnc<T>(elemento);
		if (estaVazia()) {
			this.inicio = this.fim = no;
		} else {
			NoDupEnc<T> current = this.inicio;
			for (int i = 0; i < pos - 1; i++) {
				current = current.getProximo();
			}
			current.getAnterior().setProximo(no);
			no.setAnterior(current.getAnterior());
			no.setProximo(current);
			current.setAnterior(no);
		}
		this.count++;
	}

	@Override
	public boolean contem(T elemento) {
		NoDupEnc<T> current = this.inicio;
		boolean encontrou = false;
		for (int i = 0; i < this.count; i++) {
			if (current.getElemento().equals(elemento)) {
				encontrou = true;
				break;
			}
			current = current.getProximo();
		}
		return encontrou;
	}

	@Override
	public void RemoverInicio() {
		if (!estaVazia()) {
			this.inicio = this.inicio.getProximo();
			this.inicio.setAnterior(null);
			count--;
		}
	}

	@Override
	public void RemoverFim() {
		this.fim = this.fim.getAnterior();
		this.fim.setProximo(null);
		count--;
	}

	@Override
	public void RemoverTodos(T elemento) {
		if (!estaVazia()) {
			NoDupEnc<T> current = inicio;
			int c = 0;
			for (int i = 0; i < count; i++) {
				if (current.getElemento().equals(elemento)) {
					NoDupEnc<T> anterior = current.getAnterior();
					NoDupEnc<T> proximo = current.getProximo();
					if (anterior != null && proximo != null) {
						anterior.setProximo(proximo);
					}
					if (i == 0) {
						inicio = proximo;
					}
					if (i == (count - 1)) {
						anterior.setProximo(null);
						fim = anterior;
					}
					c++;
				}
				current = current.getProximo();
			}
			count -= c;
		}
	}
	
	@Override
	public String toString() {
		String result = "[";
		NoDupEnc<T> current = inicio;
		for (int i = 0; i < this.count; i++) {
			result += current;
			if (i < this.count - 1) {
				result += ", "; 
			}
			current = current.getProximo();
		}
		result += "]";
		return result;
	}

}
