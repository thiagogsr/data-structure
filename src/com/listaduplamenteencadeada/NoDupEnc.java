package com.listaduplamenteencadeada;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

public class NoDupEnc<T> {
	
	private T elemento;
	private NoDupEnc<T> anterior;
	private NoDupEnc<T> proximo;
	
	public NoDupEnc(T elemento) {
		this.elemento = elemento;
		this.anterior = null;
		this.proximo = null;
	}
	
	public NoDupEnc(T elemento, NoDupEnc<T> anterior, NoDupEnc<T> proximo) {
		this.elemento = elemento;
		this.anterior = anterior;
		this.proximo = proximo;
	}
	
	public T getElemento() {
		return elemento;
	}
	
	public void setElemento(T elemento) {
		this.elemento = elemento;
	}
	
	public NoDupEnc<T> getAnterior() {
		return anterior;
	}
	
	public void setAnterior(NoDupEnc<T> anterior) {
		this.anterior = anterior;
	}
	
	public NoDupEnc<T> getProximo() {
		return proximo;
	}
	
	public void setProximo(NoDupEnc<T> proximo) {
		this.proximo = proximo;
	}
	
	@Override
	public String toString() {
		return elemento.toString();
	}

}
