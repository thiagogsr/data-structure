package com.listaduplamenteencadeada;

/**
 * 
 * @author Thiago Guimaraes
 *
 */

public interface IListDupEnc<T> {
	
	public NoDupEnc<T> getInicio();
	public NoDupEnc<T> getFim();
	public void clear();
	public int size();
	public boolean estaVazia();
	public void InserirInicio(T elemento);
	public void InserirFim(T elemento);
	public void Inserir(int pos, T elemento);
	public boolean contem(T elemento);
	public void RemoverInicio();
	public void RemoverFim();
	public void RemoverTodos(T elemento);
	
}
