import com.exceptions.FilaVaziaException;
import com.fila.FilaEncadeada;


/**
 * 
 * @author Thiago Guimaraes
 *
 */

public class Prova {
	
	/**
	 * Metodo para a quarta questao da prova de ED1 da segunda unidade.
	 * Sao informadas 3 listas, onde a primeira e preferencial e a terceira anda a cada duas remocoes da fila 2.
	 * @param args
	 * @throws FilaVaziaException
	 */
	public static <T> void main(String[] args) throws FilaVaziaException {
		// Exemplo 1
//		FilaEncadeada<Integer> fila1 = new FilaEncadeada<Integer>();
//		fila1.Enfileirar(1);
//		fila1.Enfileirar(2);
//		fila1.Enfileirar(3);
//		fila1.Enfileirar(4);
//		fila1.Enfileirar(5);
//		
//		FilaEncadeada<Integer> fila2 = new FilaEncadeada<Integer>();
//		fila2.Enfileirar(6);
//		fila2.Enfileirar(7);
//		fila2.Enfileirar(8);
//		fila2.Enfileirar(9);
//		
//		FilaEncadeada<Integer> fila3 = new FilaEncadeada<Integer>();
//		fila3.Enfileirar(10);
//		fila3.Enfileirar(11);
//		fila3.Enfileirar(12);
//		fila3.Enfileirar(13);
//		fila3.Enfileirar(14);
		
		// Exemplo 2
		FilaEncadeada<Integer> fila1 = new FilaEncadeada<Integer>();
		fila1.Enfileirar(1);
		fila1.Enfileirar(2);
		
		FilaEncadeada<Integer> fila2 = new FilaEncadeada<Integer>();
		fila2.Enfileirar(3);
		fila2.Enfileirar(4);
		fila2.Enfileirar(5);
		fila2.Enfileirar(6);
		fila2.Enfileirar(7);
		fila2.Enfileirar(8);
		
		FilaEncadeada<Integer> fila3 = new FilaEncadeada<Integer>();
		fila3.Enfileirar(9);
		fila3.Enfileirar(10);
		
		int tamanhoFila1 = fila1.size();
		int total = tamanhoFila1 + fila2.size() + fila3.size();
		int impressos = 0;
		
		String result = "[";
		
		for (int i = 0; i < tamanhoFila1; i++) {
			if (!fila1.estaVazia()) {
				result += fila1.Remover();
				impressos++;
				if (impressos < total) {
					result += ",";
				}
			}
		}
		
		int semPreferenciais = fila2.size() + fila3.size();
		int totalFila2 = 0;
		for (int i = 0; i < semPreferenciais; i++) {
			if (totalFila2 > 0 && totalFila2 % 2 == 0) {
				if (!fila3.estaVazia()) {
					result += fila3.Remover();
					impressos++;
					if (impressos < total) {
						result += ",";
					}
				}
			}
			
			if (!fila2.estaVazia()) {
				result += fila2.Remover();
				totalFila2++;
				impressos++;
				if (impressos < total) {
					result += ",";
				}
			}
		}
		
		result += "]";
		
		System.out.println(result);
		
	}

}
