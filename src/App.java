import java.util.Random;

import com.exceptions.FilaCheiaException;
import com.exceptions.FilaVaziaException;
import com.exceptions.PilhaVaziaException;
import com.fila.FilaEncadeada;
import com.fila.FilaVetor;
import com.hash.HashDuplo;
import com.pilha.PilhaVetor;

public class App {

	public static <T> void main(String[] args) throws FilaVaziaException, PilhaVaziaException, FilaCheiaException {
//		HashDuplo<Integer> hash = new HashDuplo<Integer>();
//		for (int i = 0; i < 10; i++) {
//			hash.add(new Random().nextInt(100));
//		}
//		System.out.println(hash);
		
//		HashDuplo<Integer> hash = new HashDuplo<Integer>(10);
//		hash.add(10);
//		System.out.println(hash);
//		hash.add(100);
//		System.out.println(hash);
//		hash.add(1000);
//		System.out.println(hash);
//		hash.add(10);
//		System.out.println(hash);
		
		PilhaVetor<Integer> pilha = new PilhaVetor<Integer>(Integer.class, 10);
		pilha.empilhe(3);
		pilha.empilhe(1);
		pilha.empilhe(4);
		System.out.println("Pilha atual: " + pilha);
		PilhaVetor<Integer> pilha2 = new PilhaVetor<Integer>(Integer.class, 10);
		pilha2.empilhe(2);
		pilha2.empilhe(8);
		pilha2.empilhe(5);
		System.out.println("Parâmetro: " + pilha2);
		System.out.println(pilha.ordenaPilha(pilha2, Integer.class));
		
//		FilaVetor<Integer> filaVetor = new FilaVetor<Integer>(Integer.class, 10);
//		filaVetor.Enfileirar(1);
//		filaVetor.Enfileirar(2);
//		filaVetor.Enfileirar(3);
//		filaVetor.Enfileirar(4);
//		filaVetor.Enfileirar(5);
//		System.out.println(filaVetor.toString(true));
//		filaVetor.Remover();
//		System.out.println(filaVetor.toString(true));
//		filaVetor.Enfileirar(6);
//		filaVetor.Enfileirar(7);
//		filaVetor.Enfileirar(8);
//		System.out.println(filaVetor.toString(true));
		
//		FilaEncadeada<Integer> fila = new FilaEncadeada<Integer>();
//		fila.Enfileirar(5);
//		fila.Enfileirar(3);
//		fila.Enfileirar(2);
//		fila.Enfileirar(1);
//		fila.Enfileirar(4);
//		System.out.println("Original: " + fila);
//		FilaEncadeada<Integer> fila2 = new FilaEncadeada<Integer>();
//		fila2.Enfileirar(4);
//		fila2.Enfileirar(3);
//		fila2.Enfileirar(9);
//		fila2.Enfileirar(2);
//		fila2.Enfileirar(6);
//		System.out.println("Parâmetro: " + fila2);
//		System.out.println("Retorno: " + fila.MesmosElementos(fila2));

//		FilaEncadeada<Integer> filaEncadeada = new FilaEncadeada<Integer>();
//		filaEncadeada.Enfileirar(1);
//		filaEncadeada.Enfileirar(2);
//		filaEncadeada.Enfileirar(3);
//		filaEncadeada.Enfileirar(4);
//		filaEncadeada.Enfileirar(5);
//		filaEncadeada.Enfileirar(6);
//		filaEncadeada.Enfileirar(7);
//		filaEncadeada.Enfileirar(8);
//		System.out.println(filaEncadeada.toString(true));
//		filaEncadeada.Remover();
//		System.out.println(filaEncadeada.toString(true));
//		filaEncadeada.Remover();
//		System.out.println(filaEncadeada.toString(true));
//		filaEncadeada.Remover();
//		System.out.println(filaEncadeada.toString(true));
//		filaEncadeada.Enfileirar(9);
//		System.out.println(filaEncadeada.toString(true));
		
//		FilaListaPrioridade<Integer> filaListaPrioridade = new FilaListaPrioridade<Integer>();
//		filaListaPrioridade.Enfileirar(1);
//		filaListaPrioridade.Enfileirar(2);
//		filaListaPrioridade.Enfileirar(3);
//		filaListaPrioridade.Enfileirar(4);
//		filaListaPrioridade.Enfileirar(5);
//		filaListaPrioridade.Enfileirar(6);
//		filaListaPrioridade.Enfileirar(7);
//		filaListaPrioridade.Enfileirar(8);
//		filaListaPrioridade.Enfileirar(9);
//		filaListaPrioridade.Enfileirar(10);
//		filaListaPrioridade.Enfileirar(11);
//		filaListaPrioridade.Enfileirar(12);
//		filaListaPrioridade.Enfileirar(13);
//		System.out.println(filaListaPrioridade.toString(true));
//		
//		Integer numero = 2;
//		Integer numero2 = 1;
//		System.out.println(numero.compareTo(numero2));
		
//		PilhaVetor<Integer> pilhaVetor = new PilhaVetor<Integer>();
//		pilhaVetor.empilhe(1);
//		pilhaVetor.empilhe(2);
//		pilhaVetor.empilhe(3);
//		pilhaVetor.empilhe(4);
//		pilhaVetor.empilhe(5);
//		pilhaVetor.empilhe(6);
//		pilhaVetor.empilhe(7);
//		pilhaVetor.empilhe(8);
//		System.out.println(pilhaVetor.toString(true));
//		pilhaVetor.desempilhe();
//		System.out.println("Desimpilhar");
//		System.out.println(pilhaVetor.toString(true));
//		
//		PilhaEncadeada<Integer> pilhaEncadeada = new PilhaEncadeada<Integer>();
//		pilhaEncadeada.empilhe(1);
//		pilhaEncadeada.empilhe(2);
//		pilhaEncadeada.empilhe(3);
//		pilhaEncadeada.empilhe(4);
//		pilhaEncadeada.empilhe(5);
//		pilhaEncadeada.empilhe(6);
//		pilhaEncadeada.empilhe(7);
//		pilhaEncadeada.empilhe(8);
//		System.out.println(pilhaEncadeada.toString(true));
//		pilhaEncadeada.desempilhe();
//		System.out.println(pilhaEncadeada.toString(true));
//		pilhaEncadeada.desempilhe();
//		System.out.println(pilhaEncadeada.toString(true));
//		pilhaEncadeada.desempilhe();
//		System.out.println(pilhaEncadeada.toString(true));
//		pilhaEncadeada.desempilhe();
//		System.out.println(pilhaEncadeada.toString(true));
		
//		Vetor<Integer> vetor = new Vetor<Integer>();
//		vetor.Adicionar(6);
//		vetor.Adicionar(8);
//		vetor.Adicionar(10);
//		vetor.Adicionar(2);
//		vetor.Adicionar(16);
//		vetor.Adicionar(4);
//		vetor.Adicionar(20);
//		vetor.Adicionar(14);
//		vetor.Adicionar(12);
//		vetor.Adicionar(18);
//		System.out.println(vetor);
//		vetor.insertionSort();
//		System.out.println(vetor);
		
//		IListaSimpEnc<Integer> lista = new ListaSimpEnc<Integer>();
//		IListaSimpEnc<Integer> lista2 = new ListaSimpEnc<Integer>();
//		
//		lista.InserirInicio(new Integer(10));
//		lista.InserirInicio(new Integer(20));
//		lista.InserirInicio(new Integer(30));
//		lista.InserirFim(new Integer(4));
//
//		System.out.println("Lista1 inicial: " + lista.toString());
//		
//		lista.RemoverInicio();
//		
//		System.out.println("Após remover do início: " + lista.toString());
//		
//		lista.RemoverFim();
//		
//		System.out.println("Após remover do fim: " + lista.toString());
//		
//		lista.Inserir(2, new Integer(5));
//		
//		System.out.println("Após inserir o 5 na posição 2: " + lista.toString());
//		
//		lista.Inserir(new Integer(5), new Integer(8));
//		
//		System.out.println("Após inserir o 8 antes do 5: " + lista.toString());
//		
//		lista.Inserir(new Integer(20), new Integer(2));
//		
//		System.out.println("Após inserir o 2 antes do 20: " + lista.toString());
//		
//		lista2.InserirInicio(new Integer(101));
//		lista2.InserirInicio(new Integer(103));
//		lista2.InserirInicio(new Integer(104));
//		
//		System.out.println("Lista 2: " + lista2.toString());
//
//		lista.Inserir(2, (ListaSimpEnc<Integer>)lista2);
//		System.out.println("Lista 2: " + lista2.getFim().getProximo());
//
//		System.out.println("Lista 1 após inserir a lista 2 antes da posição 2: " + lista.toString());
//		
//		System.out.println("PROVA");
//		IListDupEnc<Integer> prova = new ListDupEnc<Integer>();
//		prova.InserirFim(new Integer(3));
//		prova.InserirFim(new Integer(8));
//		prova.InserirFim(new Integer(3));
//		prova.InserirFim(new Integer(5));
//		prova.InserirFim(new Integer(3));
//		System.out.println(prova);
//		prova.RemoverTodos(new Integer(3));
//		System.out.println(prova);
//		System.out.println("INICIO: " + prova.getInicio());
//		System.out.println("FIM: " + prova.getFim());
//		System.out.println();
//		IVetor<Integer> vetor = new Vetor<Integer>();
//		vetor.Adicionar(new Integer(3));
//		vetor.Adicionar(new Integer(5));
//		vetor.Adicionar(new Integer(7));
//		vetor.Adicionar(new Integer(3));
//		vetor.Adicionar(new Integer(2));
//		vetor.Adicionar(new Integer(1));
//		System.out.println(vetor);
//		Integer vetor2[] = new Integer[3];
//		vetor2[0] = 15;
//		vetor2[1] = 23;
//		vetor2[2] = 48;
//		vetor.Inserir(2, vetor2);
//		System.out.println(vetor);
//		System.out.println("-- INVERTER LISTA --");
//		IListaSimpEnc<Integer> vetorInverso = new ListaSimpEnc<Integer>();
//		vetorInverso.InserirFim(new Integer(3));
//		vetorInverso.InserirFim(new Integer(5));
//		vetorInverso.InserirFim(new Integer(8));
//		System.out.println(vetorInverso);
//		vetorInverso.inverterLista();
//		System.out.println(vetorInverso);
//		System.out.println();
//		System.out.println(lista);
//		lista.inverterLista();
//		System.out.println(lista);
//		System.out.println("-- COMPARA LISTA TRUE --");
//		IListaSimpEnc<String> comparaLista1 = new ListaSimpEnc<String>();
//		IListaSimpEnc<String> comparaLista2 = new ListaSimpEnc<String>();
//		comparaLista1.InserirFim("Azul");
//		comparaLista1.InserirFim("Vermelho");
//		comparaLista1.InserirFim("Amarelo");
//		comparaLista2.InserirFim("Azul");
//		comparaLista2.InserirFim("Vermelho");
//		comparaLista2.InserirFim("Amarelo");
//		System.out.println(comparaLista1);
//		System.out.println(comparaLista2);
//		System.out.println(comparaLista1.comparaLista((ListaSimpEnc<String>) comparaLista2));
//		System.out.println("-- COMPARA LISTA FALSE --");
//		IListaSimpEnc<String> comparaLista3 = new ListaSimpEnc<String>();
//		IListaSimpEnc<String> comparaLista4 = new ListaSimpEnc<String>();
//		comparaLista3.InserirFim("Azul");
//		comparaLista3.InserirFim("Vermelho");
//		comparaLista3.InserirFim("Amarelo");
//		comparaLista4.InserirFim("Vermelho");
//		comparaLista4.InserirFim("Amarelo");
//		System.out.println(comparaLista3);
//		System.out.println(comparaLista4);
//		System.out.println(comparaLista3.comparaLista((ListaSimpEnc<String>) comparaLista4));
	}

}
